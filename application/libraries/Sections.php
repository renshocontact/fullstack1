<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sections {
  private $parent;
  public function __construct(){
    $this->parent=&get_instance();
  }
  //obtenemos las secciones d ela url con sus rutas para crear enlaces de navegación
  public function getArraySections(){
    //para manejar la ruta la separon en un arreglo mas manejable
  	$sections = $this->parent->uri->segment_array();
  	//armamos las rutas
  	$current_url='';
  	$current_section=[];
    $titles=$this->parent->lang->line('titles');
  	foreach($sections as $section){
  		if(!isset($current_url) || $current_url=='')
  			$current_url=base_url($section);
  		else
  			$current_url=$current_url.'/'.$section;
      if(isset($titles[$section]))
  		  $current_section[]=['name'=>$section,'label'=>$titles[$section],'url'=>$current_url];
  	}
    unset($current_url);
    $count=count($current_section);
    if($count>0)
      return ['data'=>$current_section,'count'=>$count];
    else
      return ['data'=>[0=>['name'=>'home','label'=>$titles['home'],'url'=>base_url('home')]],'count'=>0];
  }

  //obtenemos el titulo d ela sección, libramos al controlador de leer el arreglo y reasignarlo
  public function getTitleSection($array_title,$section){
      return $array_title[$section];
  }

  //obtenemos el ano actual, para footers por ejemplo
  public function getCurrentYear(){
    return (new DateTime( 'now',  new DateTimeZone( $this->parent->config->item('time_zone'))))->format('Y');
  }

  //obtenemos el ano actual, para footers por ejemplo
  public function getTimeZone(){
    return  (new DateTime( 'now',  new DateTimeZone( $this->parent->config->item('time_zone'))))->format('Y-m-d H:i:s');
  }
  //evaluamos si hay errores y creamos la variable con el detalle del error de haberlo
  public function getErrors($may_error){
    //vemos si hay errores
    if(validation_errors()!=''){
      return ['state'=>'fail','message'=>validation_errors()];
    }elseif(isset($may_error['state']) && $may_error['state']=='fail'){
      return ['state'=>'fail','message'=>$may_error['message']];
    }elseif(isset($may_error['state']) && $may_error['state']=='success'){
      return ['state'=>true];
    }else{
      return ['state'=>false];
    }
  }
}
?>
