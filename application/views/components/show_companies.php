    <div class="clearfix line-gradient"></div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-lg-12">
            <div class="table-responsive">
              <table class="table table-hover">
                    <thead>
                    <tr>
                      <th>
                        <form  class="form-inline" id="form-search" action="<?php echo base_url('companies');?>" method="post">
                          <label><?php echo $this->lang->line('by-name');?></label>
                          <input placeholder="" maxlength=250 class="form-control" type="text" id="name-search" name="name-search" value="<?php echo $current_search; ?>" autofocus>
                          <button class="btn btn-primary btn-lg" id="send-info" name="send-info" type="submit" ><b class="glyphicon glyphicon-search"></b></button>
                        </form>
                      </th>
                    </tr>
                    <tr>
                      <th><a href="<?php echo base_url('companies/name-'.$current_order['name']);?>"><?php echo $this->lang->line('company-name');?>&nbsp;<b class="glyphicon glyphicon-chevron-<?php echo $current_filter['name'];?>"></b></a></th>
                      <th><?php echo $this->lang->line('typology');?></th>
                      <th><a href="<?php echo base_url('companies/country-'.$current_order['country']);?>"><?php echo $this->lang->line('country');?>&nbsp;<b class="glyphicon glyphicon-chevron-<?php echo $current_filter['country'];?>"></b></a></th>
                      <th class="hidden-xs hidden-sm"><?php echo $this->lang->line('state');?></th>
                      <th class="hidden-xs hidden-sm"><?php echo $this->lang->line('city');?></th>
                      <th><?php echo $this->lang->line('user');?></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  foreach($companies['data'] as $row){
                  ?>
                    <tr>
                        <td><?php echo $row->nombre;?></td>
                        <td><?php echo $row->tipologia;?></td>
                        <td><?php echo $row->pais;?></td>
                        <td class="hidden-xs hidden-sm"><?php echo $row->estado;?></td>
                        <td class="hidden-xs hidden-sm"><?php echo $row->ciudad;?></td>
                        <td><?php echo $row->usuario;?></td>
                  <?php
                      if((float)$row->idempresa==(float)$idcompany){
                  ?>
                        <td><a href="<?php echo base_url('companies/update/');?>"><span class="glyphicon glyphicon-pencil special"></span></a></td>
                  <?php
                      }else {
                  ?>
                        <td>&nbsp;</td>
                  <?php
                      }
                  ?>
                    </tr>
                  <?php
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div>
        <div class="col-xs-12 col-lg-12"><?php echo $navigation;?></div>
      </div>


    </div>
