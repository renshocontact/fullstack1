    <div class="clearfix line-gradient"></div>
    <div class="container-fluid">
      <form id="form-register" action="<?php echo base_url('companies/init-session');?>" method="post">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <h3 class=clean><?php echo $title;?></h3>
          </div>
        </div>
        <div class="row"><div class="col-xs-12 col-lg-12 "><div class="underlined"></div></div></div>
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="container">
              <?php $this->load->view(VALIDATION.'message_result',$custom_error);?>
              <div class="col-xs-12 col-lg-6 col-lg-offset-3">
                <fieldset class="form-group">
                  <label><?php echo $this->lang->line('user');?></label>
                  <?php
                  $placeholder='';
                  $class='';
                  if(strlen(form_error('user'))>0){
                    $placeholder=$this->lang->line('required');
                    $class='has-error';
                  }
                  ?>
                  <input placeholder="<?php echo $placeholder;?>" maxlength=250 class="form-control <?php echo $class;?>" type="text" id="user" name="user" value="<?php echo $form['user']; ?>" autofocus>
                </fieldset>
              </div>
              <div class="col-xs-12 col-lg-6 col-lg-offset-3">
                <fieldset class="form-group">
                  <label><?php echo $this->lang->line('password');?></label>
                  <?php
                  $placeholder='';
                  $class='';
                  if(strlen(form_error('password'))>0){
                    $placeholder=$this->lang->line('required');
                    $class='has-error';
                  }
                  ?>
                  <input placeholder="<?php echo $placeholder;?>" maxlength=100 class="form-control <?php echo $class;?>" type="password" id="password" name="password" value="<?php echo $form['password']; ?>" autofocus>
                </fieldset>
              </div>
              <div class="row">
                <div class="col-xs-12 col-lg-12 center-block">
                  <button class="btn btn-primary btn-lg" id="send-info" name="send-info" type="submit" ><?php echo $this->lang->line('save-changes');?></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
