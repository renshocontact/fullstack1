    <div class="clearfix line-gradient"></div>
    <div class="container-fluid">
      <form id="form-register" action="<?php echo base_url($current_url);?>" method="post">
        <div class="row">
          <div class="pull-left">
            <div class="col-xs-12 col-lg-12">
              <h3 class=clean><?php echo $title;?></h3>
            </div>
          </div>
          <div class="pull-right">
            <div class="col-xs-12 col-lg-12">
              <button class="btn btn-primary btn-lg" id="send-info" name="send-info" type="submit" ><?php echo $this->lang->line('save-changes');?></button>
            </div>
          </div>
        </div>
        <div class="row"><div class="col-xs-12 col-lg-12 "><div class="underlined"></div></div></div>
        <div class="row">
          <?php $this->load->view(VALIDATION.'message_result',$custom_error);?>
          <div class="col-xs-12 col-lg-12">
              <div class=" cleancol-xs-12 col-lg-7">
                  <div class="row">
                    <h4 class="h4-gray clean"><?php echo $this->lang->line('basic-information');?></h4>
                    <div class="form-bg">
                      <div class="col-xs-12 col-lg-6">
                        <fieldset class="form-group">
                          <label ><?php echo $this->lang->line('company-name');?></label>
                          <?php
                          $placeholder='';
                          $class='';
                          if(strlen(form_error('company-name'))>0){
                            $placeholder=$this->lang->line('required');
                            $class='has-error';
                          }
                          ?>
                          <input placeholder="<?php echo $placeholder;?>" maxlength=250 class="form-control <?php echo $class;?>" type="text" id="company-name" name="company-name" value="<?php echo $form['company-name']; ?>" autofocus>
                        </fieldset>
                      </div>
                      <div class="col-xs-12 col-lg-6">
                        <fieldset class="form-group">
                          <label ><?php echo $this->lang->line('typology');?></label>
                          <select class="form-control" id="typology" name="typology">
                            <?php
                            if(isset($typologies) && $typologies!==false){
                              foreach($typologies as $row){
                            ?>
                            <option value='<?php echo $row->idtipologia;?>' <?php if(isset($form['typology']) && (float)$form['typology']==(float)$row->idtipologia) echo 'selected="selected"'; ?>><?php echo $row->descripcion;?> (<?php echo strtoupper($row->siglas);?>)</option>
                            <?php
                              }
                            }
                            ?>
                          </select>
                        </fieldset>
                      </div>

                      <div class="col-xs-12 col-lg-4">
                        <fieldset class="form-group">
                          <label ><?php echo $this->lang->line('country');?></label>
                          <select class="form-control" id="country" name="country">
                            <?php
                            if(isset($countries) && $countries!==false){
                              foreach($countries as $row){
                            ?>
                            <option value='<?php echo $row->idpais;?>' <?php if(isset($form['country']) && (float)$form['country']==(float)$row->idpais) echo 'selected="selected"'; ?>><?php echo $row->pais;?> (<?php echo ucfirst($row->continente);?>)</option>
                            <?php
                              }
                            }
                            ?>
                          </select>
                        </fieldset>
                      </div>

                      <div class="col-xs-12 col-lg-4">
                        <fieldset class="form-group">
                          <label ><?php echo $this->lang->line('state');?></label>
                          <?php
                          $placeholder='';
                          $class='';
                          if(strlen(form_error('state'))>0){
                            $placeholder=$this->lang->line('required');
                            $class='has-error';
                          }
                          ?>
                          <input placeholder="<?php echo $placeholder;?>" maxlength=100 class="form-control <?php echo $class;?>" type="text" id="state" name="state" value="<?php echo $form['state']; ?>">
                        </fieldset>
                      </div>

                      <div class="col-xs-12 col-lg-4">
                        <fieldset class="form-group">
                          <label ><?php echo $this->lang->line('city');?></label>
                          <?php
                          $placeholder='';
                          $class='';
                          if(strlen(form_error('city'))>0){
                            $placeholder=$this->lang->line('required');
                            $class='has-error';
                          }
                          ?>
                          <input placeholder="<?php echo $placeholder;?>" maxlength=100 class="form-control <?php echo $class;?>" type="text" id="city" name="city" value="<?php echo $form['city']; ?>">
                        </fieldset>
                      </div>
                    </div>
                   </div>
                   <div class="row">
                     <h4 class="h4-gray clean"><?php echo $this->lang->line('register-information');?></h4>
                     <div class="form-bg">
                       <div class="col-xs-12 col-lg-4">
                         <fieldset class="form-group">
                           <label ><?php echo $this->lang->line('mail').'/'.$this->lang->line('user');?></label>
                           <?php
                           $placeholder='';
                           $class='';
                           if(strlen(form_error('user'))>0){
                             $placeholder=$this->lang->line('required-mail');
                             $class='has-error';
                           }
                           ?>
                           <input placeholder="<?php echo $placeholder;?>" maxlength=250 class="form-control <?php echo $class;?>" type="text" id="user" name="user" value="<?php echo $form['user']; ?>">
                         </fieldset>
                       </div>
                       <div class="col-xs-12 col-lg-4">
                         <fieldset class="form-group">
                           <label ><?php echo $this->lang->line('password');?></label>
                           <?php
                           $placeholder='';
                           $class='';
                           if(strlen(form_error('password'))>0){
                             $placeholder=$this->lang->line('required-char-num');
                             $class='has-error';
                           }
                           ?>
                           <input placeholder="<?php echo $placeholder;?>" maxlength=100 class="form-control <?php echo $class;?>" type="password" id="password" name="password" value="<?php echo $form['password']; ?>">
                         </fieldset>
                       </div>

                       <div class="col-xs-12 col-lg-4">
                         <fieldset class="form-group">
                           <label ><?php echo $this->lang->line('repeat-password');?></label>
                           <?php
                           $placeholder='';
                           $class='';
                           if(strlen(form_error('repeat-password'))>0){
                             $placeholder=$this->lang->line('required-repeat-password');
                             $class='has-error';
                           }
                           ?>
                           <input placeholder="<?php echo $placeholder;?>" maxlength=100 class="form-control <?php echo $class;?>" type="password" id="repeat-password" name="repeat-password" value="<?php echo $form['repeat-password']; ?>">
                         </fieldset>
                       </div>
                     </div>
                    </div>
               </div>

               <div class="col-xs-12 col-lg-4 pull-right">
                 <h4 class="h4-gray clean">&nbsp;</h4>
                 <div class="row form-bg">

                   <div class="col-xs-12 col-lg-12">
                     <fieldset class="form-group">
                       <label ><?php echo $this->lang->line('company-description');?></label>
                       <?php
                       $placeholder='';
                       $class='';
                       if(strlen(form_error('company-description'))>0){
                         $placeholder=$this->lang->line('required');
                         $class='has-error';
                       }
                       ?>
                       <textarea maxlength=1000 placeholder="<?php echo $placeholder;?>" id="company-description" name="company-description" class="clean form-control <?php echo $class;?>" rows="14" ><?php echo $form['company-description']; ?></textarea>
                     </fieldset>
                   </div>
                  </div>
                </div>
          </div>
        </div>
      </form>
    </div>
