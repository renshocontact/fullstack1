<header class="container">
  <div class = "row">
    <div class = "col-xs-12 col-lg-12 clearfix">
      <nav class = "navbar navbar-default" role = "navigation">
       <div class = "navbar-header">
          <button type = "button" class = "navbar-toggle" data-toggle = "collapse" data-target = "#menu-nav">
             <span class = "sr-only">Toggle navigation</span>
             <span class = "icon-bar"></span>
             <span class = "icon-bar"></span>
             <span class = "icon-bar"></span>
          </button>
       </div>
       <div class = "collapse navbar-collapse" id = "menu-nav">
          <ul class = "nav navbar-nav">
             <?php if(isset($current_section['data'][$current_section['count']-1]['name']) && $current_section['data'][$current_section['count']-1]['name']==='home'  ){ ?>
             <li class="active"><a  href = "<?php echo base_url('home');?>"><?php echo $this->lang->line('home');?></a></li>
             <?php }else{ ?>
             <li><a href = "<?php echo base_url('home');?>"><?php echo $this->lang->line('home');?></a></li>
             <?php } ?>
             <li class="separator end hidden-xs hidden-sm" ><a>&#124;</a></li>
             <?php if(isset($current_section['data'][$current_section['count']-1]['name']) && $current_section['data'][$current_section['count']-1]['name']==='companies'  ){ ?>
             <li class="active"><a  href = "<?php echo base_url('companies');?>"><?php echo $this->lang->line('companies');?></a></li>
             <?php }else{ ?>
             <li><a href = "<?php echo base_url('companies');?>"><?php echo $this->lang->line('companies');?></a></li>
             <?php } ?>
             <li class="separator end hidden-xs hidden-sm" ><a>&#124;</a></li>
             <?php if(isset($current_section['data'][$current_section['count']-1]['name']) && $current_section['data'][$current_section['count']-1]['name']==='services'  ){ ?>
             <li class="active"><a  href = "<?php echo base_url('services');?>"><?php echo $this->lang->line('services');?></a></li>
             <?php }else{ ?>
             <li><a href = "<?php echo base_url('services');?>"><?php echo $this->lang->line('services');?></a></li>
             <?php } ?>
             <li class="separator end hidden-xs hidden-sm" ><a>&#124;</a></li>
             <?php if(isset($current_section['data'][$current_section['count']-1]['name']) && $current_section['data'][$current_section['count']-1]['name']==='price'  ){ ?>
             <li class="active"><a  href = "<?php echo base_url('price');?>"><?php echo $this->lang->line('price');?></a></li>
             <?php }else{ ?>
             <li><a href = "<?php echo base_url('price');?>"><?php echo $this->lang->line('price');?></a></li>
             <?php } ?>
             <li class="separator end hidden-xs hidden-sm" ><a>&#124;</a></li>
             <?php if(isset($current_section['data'][$current_section['count']-1]['name']) && $current_section['data'][$current_section['count']-1]['name']==='contact'  ){ ?>
             <li class="active"><a  href = "<?php echo base_url('contact');?>"><?php echo $this->lang->line('contact');?></a></li>
             <?php }else{ ?>
             <li class='end'><a href = "<?php echo base_url('contact');?>"><?php echo $this->lang->line('contact');?></a></li>
             <?php } ?>
          </ul>
          <?php
          $session_data=$this->session->userdata('user-login');
      		if($session_data!=NULL && $session_data!=''){
          ?>
          <ul class = "nav navbar-nav navbar-right nav-color">
            <li class="end"><a href = "<?php echo base_url('companies/update');?>"><?php echo $this->lang->line('update');?></a></li>
            <li class="end hidden-xs hidden-sm" ><a   href = "#">&#124;</a></li>
            <li class="end"><a data-header="<?php echo $this->lang->line('close-session-header');?>" data-cancel="<?php echo $this->lang->line('close-session-cancel');?>" data-confirm="<?php echo $this->lang->line('close-session-confirm');?>" data-confirm-content="<?php echo $this->lang->line('close-session-confirm-content');?>" href = "<?php echo base_url('companies/close-session');?>"><?php echo $this->lang->line('close-session');?></a></li>
          </ul>
          <?php
          }else {
          ?>
          <ul class = "nav navbar-nav navbar-right nav-color">
            <li class="end"><a href = "<?php echo base_url('companies/register');?>"><?php echo $this->lang->line('register');?></a></li>
            <li class="end hidden-xs hidden-sm" ><a   href = "#">&#124;</a></li>
            <li class="end"><a  href = "<?php echo base_url('companies/init-session');?>"><?php echo $this->lang->line('init-session');?></a></li>
          </ul>
          <?php
          }
          ?>
       </div>
      </nav>
      <section class="sections">
        <?php if(isset($current_section) && $current_section!==false){?>
        <div class="section"><?php echo $this->lang->line('where');?>:
          <?php $count=$current_section['count']-1; ?>
          <?php foreach($current_section['data'] as $key => $info):?>
            <?php if($count!==$key && $count>0){ ?>
            <a href='<?php echo $info['url'];?>'><?php echo $info['label'];?></a><span class="glyphicon glyphicon-chevron-right"></span>
            <?php } ?>
          <?php endforeach;?>
          <span class="current-section"><?php echo $info['label'];?></span>
        <?php } ?>
        </div>
      </section>
</header>
