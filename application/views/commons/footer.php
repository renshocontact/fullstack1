<footer class="container">
  <div class = "row">
    <div class = "col-xs-12 col-lg-12">
      <div class="clearfix line-gradient"></div>
      <div class="container-fluid">
        <div class="row footer-menu">
          <div class="col-xs-12 col-lg-10 col-lg-offset-2">
            <div class="row">
              <div class="col-xs-6 col-lg-3">
                <h5><a href = "#"><?php echo ucfirst(strtolower($this->lang->line('company')));?></a></h4>
                <div class="list-menu">
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                </div>
              </div>
              <div class="col-xs-6 col-lg-3">
                <h5><a href = "#"><?php echo ucfirst(strtolower($this->lang->line('services')));?></a></h5>
                <div class="list-menu">
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                </div>
              </div>
              <div class="col-xs-6 col-lg-3">
                <h5><a href = "#"><?php echo ucfirst(strtolower($this->lang->line('price')));?></a></h5>
                <div class="list-menu">
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                </div>
              </div>
              <div class="col-xs-6 col-lg-3">
                <h5><a href = "#"><?php echo ucfirst(strtolower($this->lang->line('contact')));?></a></h5>
                <div class="list-menu">
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                  <p><?php echo $this->lang->line('content');?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row footer-menu">
            <div class="col-xs-12 col-lg-9 col-lg-offset-2">
              <h4>&copy;&nbsp;<?php echo $current_year;?>&nbsp;<?php echo ucfirst(strtolower($this->lang->line('opinion')));?></h4>
              <p><?php echo $this->lang->line('copyright');?></p>
            </div>
        </div>
        <div class="row footer-menu">
            <div class="center-block">
              <h4 class="h4-gray"><?php echo ucfirst(strtolower($this->lang->line('own')));?>&nbsp;&copy;&nbsp;<?php echo $current_year;?></h4>
            </div>
        </div>
      </div>
    </div>
  </div>
</footer>
