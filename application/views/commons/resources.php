<link rel="stylesheet" href="<?php echo base_url(BOOTSTRAP_STYLE_PATH);?>">
<link rel="stylesheet" href="<?php echo base_url(PAGE_STYLE_PATH);?>">
<link rel="stylesheet" href="<?php echo base_url(BOOTSTRAP_FIX_STYLE_PATH);?>">
<script src="<?php echo base_url(JQUERY_PATH);?>"></script>
<script src="<?php echo base_url(SCRIPTS_PATH.'modal.js');?>"></script>

<script src="<?php echo base_url(BOOTSTRAP_JS_PATH);?>"></script>
