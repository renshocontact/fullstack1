<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>TEST- <?php echo $title;?></title>
  <?php $this->load->view(COMMONS_VIEWS.'metas');?>
  <?php $this->load->view(COMMONS_VIEWS.'resources');?>
</head>
<body>
	<?php $this->load->view(COMMONS_VIEWS.'header');?>
  <section class="container">
    <div class="row">
      <div class="col-xs-12 col-lg-12">
        <div class="container-fluid jumbotron">
          <h1>Página de Inicio</h1>
          <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile-first projects on the web.</p>
          <p><a href="<?php echo site_url('companies/register');?>" class="btn btn-primary btn-lg" role="button"><?php echo $this->lang->line('register');?></a></p>
        </div>
      </div>
    </div>
  </section>
  <?php $this->load->view(COMMONS_VIEWS.'footer');?>
</body>
</html>
