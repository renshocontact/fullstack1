<?php
if(isset($state) && $state!==false  && $state!==true){
?>
<div class="col-xs-12 col-lg-6 col-lg-offset-3 alert alert-danger fade in">
  <a href="#" class="close" data-dismiss="alert">&times;</a>
  <b class="glyphicon glyphicon-warning-sign"></b>&nbsp;&nbsp;<?php echo $message; ?></div>
<?php
}else{
  if(isset($state) && $state===true){
?>
<div class="col-xs-8 col-xs-offset-2 col-lg-6 col-lg-offset-3 alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <b class="glyphicon glyphicon-floppy-saved"></b>&nbsp;&nbsp;<?php echo $this->lang->line('success-insert');?>
</div>
<?php
  }
}
?>
