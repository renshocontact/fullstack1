<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>TEST- <?php echo $title;?></title>
  <?php $this->load->view(COMMONS_VIEWS.'metas');?>
  <?php $this->load->view(COMMONS_VIEWS.'resources');?>
</head>
<body>
	<?php $this->load->view(COMMONS_VIEWS.'header');?>
  <section class="container">
      <div class="row">
        <div class="col-xs-12 col-lg-12">
          <?php $this->load->view(COMPONENTS_VIEWS.'form_company');?>
        </div>
      </div>
  </section>
  <?php $this->load->view(COMMONS_VIEWS.'footer');?>
</body>
</html>
