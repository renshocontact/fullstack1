<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Consult extends CI_Controller {
	public function index(){}
	public function showCompanies($page,$params)
	{
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database();
		//si hay sesion abierta no pemritimos estar aca redireccionamos
		$session_data=$this->session->userdata('user-login');
		if($session_data==NULL || $session_data=='')
			redirect('home');
		//libreria con lo necesario para mostrar contenidos particulares en la página, ahorro de código y más fácil mantenimiento
		$this->load->library('sections');
		//lenguaje
		$this->lang->load('content_page');
		//interpretamos param, separamos los valores y obtenemos lo que necesitamos
		//limpiamos la url quitando el page
		$separated_url=explode('/',str_replace($page.'/','',uri_string()));
		$name_search='';
		$orderName=0;
		$orderCountry=0;
		//el filtro que actualmente se selecionó por parte del usuario
		$filters='';
		//variable que indica a la vista que la accion de filtro debe activarse es contraria a la seleccionada, por defecto es asc
		$data['current_filter']=['country'=>'up','name'=>'up'];
		$data['current_search']='';
		$data['current_order']=['country'=>'asc','name'=>'asc'];
		//recorremops cada segmento de la ruta, desde 1 ya que 0 es companies
		for($i=1;$i<count($separated_url);++$i){
			//deberia estar un guion en la sección evaluada, es normal de las url aqui
			$col_url=explode('-',$separated_url[$i]);
			//si no habvia guion nunca sera dos, en ese caso no hacemos nada
			if(count($col_url)==2){
				//martiz que contiene las posbilidades, incluyendo el truco del inverso para los filtros, tambiem esta lo del inverso de order
				$convert=['order'=>['asc'=>'1', 'desc'=>'2'],'filter'=>['asc'=>'down', 'desc'=>'up'],'order_filter'=>['asc'=>'desc', 'desc'=>'asc']];
				if($col_url[1]=='asc' || $col_url[1]=='desc'){
					//vemos la posición cero (izquierda del guion) sólo admitimos search,country y typology
					if($col_url[0]=='country'){
						$orderCountry=$convert['order'][$col_url[1]];
						if(trim($filters)==='')
							$filters='country-'.$col_url[1];
						else
							$filters=$filters.'/country-'.$col_url[1];
						$data['current_order']['country']=$convert['order_filter'][$col_url[1]];
						$data['current_filter']['country']=$convert['filter'][$col_url[1]];
					}elseif($col_url[0]=='name'){
						$orderName=$convert['order'][$col_url[1]];
						if(trim($filters)==='')
							$filters='name-'.$col_url[1];
						else
							$filters=$filters.'/name-'.$col_url[1];
						$data['current_order']['name']=$convert['order_filter'][$col_url[1]];
						$data['current_filter']['name']=$convert['filter'][$col_url[1]];
					}
				}elseif($col_url[0]=='search'){
					$name_search=$col_url[1];
					$data['current_search']=$col_url[1];
					if(trim($filters)==='')
						$filters='search-'.$col_url[1];
					else
						$filters=$filters.'/search-'.$col_url[1];
				}
			}
		}
		//manejamos el search si viene por post, sobreescribimos a lo que viene por get y todo los otros filtros
		if(trim($this->input->post('name-search'))!==''){
			$name_search=trim($this->input->post('name-search'));
			$data['current_search']=$name_search;
			$filters='search-'.$name_search;
			$data['current_filter']=['country'=>'up','name'=>'up'];			
			$data['current_order']['name']='asc/search-'.$data['current_search'];
			$data['current_order']['country']='asc/search-'.$data['current_search'];
		}else {
			//esto e suna salñvead para asegurar que los enlaces de asc y desc conserven el search
			$data['current_order']['name']=$data['current_order']['name'].'/search-'.$data['current_search'];
			$data['current_order']['country']=$data['current_order']['country'].'/search-'.$data['current_search'];
		}

		$data['current_section']=$this->sections->getArraySections();
		$data['title']=$this->sections->getTitleSection($this->lang->line('titles'),'companies');
		$data['current_year']=$this->sections->getCurrentYear();
		$this->load->model(COMPANIES_FOLDER.'Companies_query','companies_query');
		$data['companies']=$this->companies_query->getCompanies($name_search,$page,MAX_ROWS,$orderName,$orderCountry);
		//la cantidad esencial para la paginación
		$data['count_companies']=$this->companies_query->getCountCompanies($name_search,$orderName,$orderCountry);
		$this->load->library('pagination');

		$config=[
		'base_url' => base_url('companies/'.$filters),
		'full_tag_open' => '<div class="center-block">','full_tag_close' => '</div>',
		'last_tag_open' => '<button type = "button" class = "btn btn-primary">','last_tag_close' => '</button>',
		'next_tag_open' => '<button type = "button" class = "btn btn-primary">','next_tag_close' => '</button>',
		'prev_tag_open' => '<button type = "button" class = "btn btn-primary">','prev_tag_close' => '</button>',
		'cur_tag_open'  => '<button type = "button" class = "btn btn-default disabled">','cur_tag_close' => '</button>',
		'num_tag_open'  => '<button type = "button" class = "btn btn-primary">','num_tag_close' => '</button>',
		'per_page'=> MAX_ROWS];
		if(isset($data['count_companies']['data']['count']))
			$config['total_rows'] = $data['count_companies']['data']['count'];
		else
			$config['total_rows'] = 0;
		//si hay modulo, la pagina suministrada por get es irregular, quizas escrita mal por el usuario, redireccionamos, tambien si el parametro GET es mayor al total redireccionamos
		if(($page%MAX_ROWS)!=0 ||  $config['total_rows']<$page)
			redirect('companies');

		$this->pagination->initialize($config);
		//ejecutamos la paginación para generar el html y usarlo en la vista donde se necesite
		$data['navigation']=$this->pagination->create_links();
		//el id de la empresa del usuario lo emplearemos en la vista
		$data['idcompany']=$session_data['idcompany'];
		$this->load->view(VIEWS_FOLDER.COMPANIES_FOLDER.'consult',$data);
	}
}
