<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sessions extends CI_Controller {
	public function index(){
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('url');
		//si hay sesion abierta no pemritimos estar aca redireccionamos
		$session_data=$this->session->userdata('user-login');
		if($session_data!=NULL && $session_data!='')
			redirect('home');
		$this->load->database();
		//libreria con lo necesario para mostrar contenidos particulares en la página, ahorro de código y más fácil mantenimiento
		$this->load->library('sections');
		$result=$this->validateSession();
		$this->openSession($result);
		$this->db->close();
	}

	public function validateSession(){
		$form=[
			'user'=>trim($this->input->post('user')),
			'password'=>trim($this->input->post('password'))
		];
		//lenguaje
		$this->lang->load('error');
		$config = [
			['field' => 'user','label' => 'lang:user','rules' => 'required|valid_email'],
			['field' => 'password','label' => 'lang:password','rules' => 'required|alpha_numeric|min_length[6]']
		];
		$this->form_validation->set_rules($config);
		if($this->form_validation->run()!== false){
			//aqui hacemos el insert
			$this->load->model(COMPANIES_FOLDER.'Companies_query','companies');
			$result= $this->companies->getUserCompany($form);
			if(isset($result['state']) && $result['state']=='success')
				redirect('/');
			else
				return $result;
    }else{
    	return ['state'=>false,'data'=>$form];
    }
	}

	private function openSession($result){
		//lenguaje
		$this->lang->load('content_page');
		$data['current_section']=$this->sections->getArraySections();
		$data['title']=$this->sections->getTitleSection($this->lang->line('titles'),'session');
		$data['current_year']=$this->sections->getCurrentYear();
		$data['custom_error']=$this->sections->getErrors($result);
		$data['form']=$result['data'];
		$this->load->view(VIEWS_FOLDER.COMPANIES_FOLDER.'session',$data);
	}
//funcion exclusiva de cierre de sesión
	public function close(){
		$this->load->library('session');
		$this->load->helper('url');
		//si hay sesion abierta cerramos y redirigimos
		$session_data=$this->session->userdata('user-login');
		if($session_data!=NULL && $session_data!=''){
			$this->session->unset_userdata('user-login');
		}
		redirect('home');
	}
}
