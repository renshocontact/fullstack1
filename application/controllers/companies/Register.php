<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller {
	public function index(){}

	public function sendForm($success){
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('url');
		//si hay sesion abierta no pemritimos estar aca redireccionamos
		$session_data=$this->session->userdata('user-login');
		if($session_data!=NULL && $session_data!='')
			redirect('home');
		$this->load->database();
		$this->load->model(TYPOLOGIES_FOLDER.'Typologies_query','typologies');
		$this->load->model(COUNTRIES_FOLDER.'Countries_query','countries');
		//libreria con lo necesario para mostrar contenidos particulares en la página, ahorro de código y más fácil mantenimiento
		$this->load->library('sections');
		$result=$this->validateRegister($success);
		$this->openRegister($result);
		$this->db->close();
	}

	public function validateRegister($success){
		$form=[
			'typology'=>trim($this->input->post('typology')),
			'country'=>trim($this->input->post('country')),
			'company-name'=>trim($this->input->post('company-name')),
			'state'=>trim($this->input->post('state')),
			'city'=>trim($this->input->post('city')),
			'user'=>trim($this->input->post('user')),
			'password'=>trim($this->input->post('password')),
			'company-description'=>trim($this->input->post('company-description'))
		];
		if(isset($success) && $success==true){
			$form['repeat-password']=trim($this->input->post('repeat-password'));
			return ['state'=>'success','data'=>$form];
		}else{
			//lenguaje
			$this->lang->load('error');
			$config = [
		    ['field' => 'typology','label' => 'lang:typology','rules' => 'required|numeric'],
				['field' => 'country','label' => 'lang:country','rules' => 'required|numeric'],
				['field' => 'company-name','label' => 'lang:company-name','rules' => 'required|min_length[3]'],
				['field' => 'state','label' => 'lang:state','rules' => 'required|alpha_numeric'],
				['field' => 'city','label' => 'lang:city','rules' => 'required|alpha_numeric'],
				['field' => 'user','label' => 'lang:user','rules' => 'required|valid_email'],
				['field' => 'password','label' => 'lang:password','rules' => 'required|alpha_numeric|min_length[6]'],
				['field' => 'repeat-password','label' => 'lang:repeat-password','rules' => 'required|matches[password]'],
				['field' => 'company-description','label' => 'lang:company-description','rules' => 'min_length[6]']
			];

			$this->form_validation->set_rules($config);
			if($this->form_validation->run()!== false){
				$this->load->model(COMPANIES_FOLDER.'Companies_query','companies_query');
				$result= $this->companies_query->getUserExist(trim($form['user']));
				//no debimos haber encontrada nada para proseguir
				if(!isset($result['state']) || $result['state']!=='success'){
					//aqui hacemos el insert
					$this->load->model(COMPANIES_FOLDER.'Companies_insert','companies');
					$form['register_date']= $this->sections->getTimeZone();
					$result= $this->companies->insertCompany($form);
					if(isset($result['state']) && $result['state']=='success')
						redirect('companies/register/success');
					else
						return $result;
				}else {
					$form['repeat-password']=trim($this->input->post('repeat-password'));
					return ['state'=>'fail','data'=>$form,'message'=>$this->lang->line('user-found')];
				}
	    }else{
				$form['repeat-password']=trim($this->input->post('repeat-password'));
	    	return ['state'=>false,'data'=>$form];
	    }
		}
	}

	private function openRegister($result){
		//lenguaje
		$this->lang->load('content_page');
		$data['typologies']=$this->typologies->getTypologies();
		$data['countries']=$this->countries->getCountries();
		$data['current_section']=$this->sections->getArraySections();
		$data['title']=$this->sections->getTitleSection($this->lang->line('titles'),'register');
		$data['current_year']=$this->sections->getCurrentYear();
		$data['custom_error']=$this->sections->getErrors($result);
		//indicamos la url del formulario
		$data['current_url']='companies/register';
		$data['form']=$result['data'];
		$this->load->view(VIEWS_FOLDER.COMPANIES_FOLDER.'register',$data);
	}
}
