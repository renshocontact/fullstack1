<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	public function index()
	{
		$this->load->library('session');
		$this->load->helper('url');
		//libreria con lo necesario para mostrar contenidos particulares en la página, ahorro de código y más fácil mantenimiento
		$this->load->library('sections');
		//lenguaje
		$this->lang->load('content_page');
		$data['current_section']=$this->sections->getArraySections();
		$data['title']=$this->sections->getTitleSection($this->lang->line('titles'),'home');
		$data['current_year']=$this->sections->getCurrentYear();
		$this->load->view('home',$data);
	}
}
