<?php
/**
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['company-name'] = 'Nombre de la empresa';
$lang['typology'] = 'Tipolog&iacute;a';
$lang['country'] = 'Pa&iacute;s';
$lang['state'] = 'Estado';
$lang['city'] = 'Ciudad';
$lang['user'] = 'Nombre de usuario (Debe ser su correo electr&oacute;nico)';
$lang['password'] = 'Password';
$lang['repeat-password'] = 'La confirmaci&oacute;n del password';
$lang['company-description'] = 'Debe indicar la descripci&oacute;n de la empresa usando al menos 6 caracteres';
$lang['user-not-found'] = 'Usuario no encontrado';
$lang['user-found'] = 'Otro usuario esta usando el usuario indicado';
$lang['company-not-found'] = 'Compa&ntilde;&iacute;a no encontrado';
$lang['companies-not-found'] = 'No se encontraron compa&ntilde;&iacute;as.';
$lang['internal-error'] = 'No se encontraron compa&ntilde;&iacute;as.';
