<?php
class Companies_query extends CI_Model {
  private $parent;
    public function __construct(){
    parent::__construct();
    $this->parent=& get_instance();
  }
  public function getCompany($idcompany){
    //por lo menso contamos las columnas para ver si estan las que se necesitan
    if(is_numeric($idcompany)){
      $result=$this->db->query('call consultar_empresa('.$idcompany.');');
      if($result->num_rows()>0){
        $return;
        foreach ($result->result() as $row){
          $return=[
            'idcompany'  =>(float)$row->idempresa,
            'idtypology' =>(float)$row->idtipologia,
            'idcountry'  =>(float)$row->idpais,
            'name'       =>$row->nombre,
            'state'      =>$row->estado,
            'city'       =>$row->ciudad,
            'user'       =>$row->usuario,
            'description'=>$row->descripcion,
            'registered' =>$row->fecha_registro,
            'updated'    =>$row->fecha_actualizacion
          ];
        }
        $result->free_result();
        return ['state'=>'success','data'=>$return];
      }else {
        $result->free_result();
        return ['state'=>'fail','data'=>$update,'message'=>$this->lang->line('company-not-found')];
      }
    }else{
      return ['state'=>'fail','message'=>'It is not a number.'];
    }
  }

  public function getUserCompany($update){
    //por lo menso contamos las columnas para ver si estan las que se necesitan
    if(count($update==8)){
      //guardamos una copia para escaparla y usarla, pero nos conviene tener una copia para regresarla en caso de error
      $update_copy=$update;
      //al password hay que hacerle un encrypt, uso sha512 y lo aplico 2 veces para complejizarlo
      $update_copy['password']=hash('sha512',hash('sha512',$update['password']));
      //escapamos los valores
      foreach($update_copy as $key => $val)
        $update_copy[$key]=$this->db->escape($val);
      $params=implode(',',$update_copy);
      $result=$this->db->query('call consultar_usuario_compania('.$params.');');
      if($result->num_rows()>0){
        $session_data=['user-login'];
        foreach ($result->result() as $row){
          $session_data['user-login']=[
            'idcompany' =>(float)$row->idempresa,
            'name'      =>$row->nombre,
            'user'      =>$row->usuario,
            'idcountry' =>(float)$row->idpais,
            'state'     =>$row->estado,
            'city'      =>$row->ciudad
          ];
        }
        //armamos la sesion
        $this->parent->session->set_userdata($session_data);
        //limpiamos el arreglo para devolverlo sin valores
        foreach($update as $key => $val)
          $update[$key]='';
        return ['state'=>'success','data'=>$update];
      }else {
        return ['state'=>'fail','data'=>$update,'message'=>$this->lang->line('user-not-found')];
      }
    }else{
      return ['state'=>'fail','message'=>'There is not enough fields.'];
    }
  }
  //funcion para verificar si existe o no el usuario pasado como parametro
  public function getUserExist($user){
    //por lo menso contamos las columnas para ver si estan las que se necesitan
    if(trim($user)!==''){
      $result=$this->db->query('call consultar_usuario_existencia('.$this->db->escape($user).');');
      if($result->num_rows()>0){
        //la unica fila que devolvio con el id que necesitamos
        $rowExist = $result->row(0);
        return ['state'=>'success','data'=>['idcompany'=>$rowExist->idempresa]];
      }else {
        return ['state'=>'fail','message'=>$this->lang->line('user-not-found')];
      }
    }else{
      return ['state'=>'fail','message'=>'The field is empty.'];
    }
  }
  //funcion que consulta las empresas de forma parametrizada según la interacción del usuario, uso variables y no un arreglo como parametro porque requiero de esas variables como columnas engordarian el codigo
  public function getCompanies($name_search='',$page=0,$count=12,$orderName=0,$orderCountry=0){
    $result = $this->customCompanies('consultar_empresa_avanzada',$name_search,$page,$count,$orderName,$orderCountry);
    if($result!==false && $result->num_rows()>0){
      //retornamos de una vez el result, puede ser pesada la consulta, que la vista la recorra unicamente para mejorar performance
      return ['state'=>'success','data'=>$result->result()];
    }else {
      return ['state'=>'fail','message'=>$this->lang->line('companies-not-found')];
    }
  }
  //función igual a la anterior salvo que esta devuelve cantidad de registros, no registros como tal
  public function getCountCompanies($name_search='',$orderName=0,$orderCountry=0){
    $result = $this->customCompanies('consultar_cantidad_empresa_avanzada',$name_search,-1,-1,$orderName,$orderCountry);
    if($result!==false && $result->num_rows()>0){
      $rowCount = $result->row(0);
      //retornamos de una vez el result, puede ser pesada la consulta, que la vista la recorra unicamente para mejorar performance
      return ['state'=>'success','data'=>['count'=>$rowCount->cantidad]];
    }else {
      return ['state'=>'fail','message'=>$this->lang->line('companies-not-found')];
    }
  }

  private function customCompanies($procedure='',$name_search='',$page=-1,$count=-1,$orderName=0,$orderCountry=0){
    if(trim($name_search)!=='')
      $name_search=trim($name_search);
    if(is_numeric(trim($page)))
      $page=(float)trim($page);
    else
      $page=0;
    if(is_numeric(trim($count)))
      $count=(float)trim($count);
    else
      $count=0;
    if(is_numeric(trim($orderName)))
      $orderName=(float)trim($orderName);
    else
      $orderName=0;
    if(is_numeric(trim($orderCountry)))
      $orderCountry=(float)trim($orderCountry);
    else
      $orderCountry=0;
    if(trim($procedure)!=='' && (trim($procedure)=='consultar_empresa_avanzada' || trim($procedure)=='consultar_cantidad_empresa_avanzada' )){
      $sql='call '.$procedure.'('.$this->db->escape($name_search);
      if($page>=0)
        $sql=$sql.','.$this->db->escape($page);
      if($count>=0)
        $sql=$sql.','.$this->db->escape($count);
      $sql=$sql.','.$this->db->escape($orderName).','.$this->db->escape($orderCountry).');';
      return $this->db->query($sql);
    }else{
      return ['state'=>'fail','message'=>$this->lang->line('internal-error')];
    }
  }
}
?>
