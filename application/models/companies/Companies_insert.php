<?php
class Companies_insert extends CI_Model {
  public function __construct(){
    parent::__construct();
  }

  public function insertCompany($insert){
    //por lo menso contamos las columnas para ver si estan las que se necesitan
    if(count($insert==8)){
      //al password hay que hacerle un encrypt, uso sha512 y lo aplico 2 veces para complejizarlo
      $insert['password']=hash('sha512',hash('sha512',$insert['password']));
      //escapamos los valores
      foreach($insert as $key => $val)
        $insert[$key]=$this->db->escape($val);
      $params=implode(',',$insert);
      $this->db->query('call insertar_compania('.$params.');');
      //limpiamos el arreglo para devolverlo sin valores
      foreach($insert as $key => $val)
        $insert[$key]='';
      //este campo puede ser necesitado fuera del la sql
      $insert['repeat-password']='';
      return ['state'=>'success','data'=>$insert];
    }else{
      return ['state'=>'fail','message'=>'There is not enough fields.'];
    }
  }
}
?>
