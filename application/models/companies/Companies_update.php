<?php
class Companies_update extends CI_Model {
  public function __construct(){
    parent::__construct();
  }

  public function updateCompany($update,$idcompany){
    //por lo menso contamos las columnas para ver si estan las que se necesitan
    if(count($update==9)){
      //al password hay que hacerle un encrypt, uso sha512 y lo aplico 2 veces para complejizarlo
      $update['password']=hash('sha512',hash('sha512',$update['password']));
      //escapamos los valores
      foreach($update as $key => $val)
        $update[$key]=$this->db->escape($val);
      $params=implode(',',$update);
      $this->db->query('call actualizar_compania('.$idcompany.','.$params.');');
      //limpiamos el arreglo para devolverlo sin valores
      foreach($update as $key => $val)
        $update[$key]='';
      //este campo puede ser necesitado fuera del la sql
      $update['repeat-password']='';
      return ['state'=>'success','data'=>$update];
    }else{
      return ['state'=>'fail','message'=>'There is not enough fields.'];
    }
  }

}
?>
