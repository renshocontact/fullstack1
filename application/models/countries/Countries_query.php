<?php
class Countries_query extends CI_Model {
  public function __construct(){
    parent::__construct();
  }
  public function getCountries(){
    $query = $this->db->query('SELECT * FROM view_paises;');
    if($query->num_rows()>0){
      $result=$query->result();
      $query->free_result();
      return $result;
    }else{
      return false;
    }
  }
}
?>
