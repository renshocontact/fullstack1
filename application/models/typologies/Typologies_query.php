<?php
class Typologies_query extends CI_Model {
  public function __construct(){
    parent::__construct();
  }
  public function getTypologies(){    
    $query = $this->db->query('select * from view_tipologias;');
    if($query->num_rows()>0){
      $result=$query->result();
      $query->free_result();
      return $result;
    }else{
      return false;
    }
  }
}
?>
