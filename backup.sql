/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.6.17 : Database - develoop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`develoop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `develoop`;

/*Table structure for table `continentes` */

DROP TABLE IF EXISTS `continentes`;

CREATE TABLE `continentes` (
  `idcontinente` double NOT NULL AUTO_INCREMENT COMMENT 'Clave principal de la tabla',
  `nombre` varchar(250) NOT NULL COMMENT 'Nombre del continente',
  PRIMARY KEY (`idcontinente`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `continentes` */

insert  into `continentes`(`idcontinente`,`nombre`) values (1,'Ninguno'),(2,'Óceanía'),(3,'África'),(4,'Asia'),(5,'Europa'),(6,'Oceanía'),(7,'América'),(8,'Antártida');

/*Table structure for table `empresas` */

DROP TABLE IF EXISTS `empresas`;

CREATE TABLE `empresas` (
  `idempresa` double NOT NULL AUTO_INCREMENT COMMENT 'Clave principal de la tabla',
  `idtipologia` double NOT NULL COMMENT 'id de la tipologia d ela empresa¿, hace referencia a la tabla tipologias',
  `idpais` double NOT NULL COMMENT 'id del pais, hace referencia a la tabla paises',
  `nombre` varchar(250) NOT NULL COMMENT 'nombre de la empresa',
  `estado` varchar(100) NOT NULL COMMENT 'estado donde se ubica la empresa',
  `ciudad` varchar(100) NOT NULL COMMENT 'ciudad donde se ubica la empresa',
  `usuario` varchar(250) NOT NULL COMMENT 'usuario o correo para uso del sistema',
  `password` varchar(200) NOT NULL COMMENT 'clave de acceso del usuario, encriptada',
  `descripcion` text COMMENT 'descripción o detalle de la empresa',
  `fecha_registro` datetime NOT NULL COMMENT 'Fecha en que fue registrada la tupla',
  `fecha_actualizacion` datetime DEFAULT NULL COMMENT 'Fecha en que fue actualizada la tupla',
  PRIMARY KEY (`idempresa`),
  KEY `idtipologia` (`idtipologia`),
  KEY `idpais` (`idpais`),
  CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`idtipologia`) REFERENCES `tipologias` (`idtipologia`),
  CONSTRAINT `empresas_ibfk_2` FOREIGN KEY (`idpais`) REFERENCES `paises` (`idpais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `empresas` */

/*Table structure for table `paises` */

DROP TABLE IF EXISTS `paises`;

CREATE TABLE `paises` (
  `idpais` double NOT NULL AUTO_INCREMENT COMMENT 'Clave principal de la tabla',
  `idcontinente` double NOT NULL COMMENT 'id del continente asociado al país, hace referencia a la tabla continentes',
  `pais` varchar(250) NOT NULL COMMENT 'Nombre del país',
  `iso2` varchar(2) NOT NULL COMMENT 'código ISO internacional de dos dígitos del país',
  `iso3` varchar(3) NOT NULL COMMENT 'código ISO internacional de tres dígitos del país',
  PRIMARY KEY (`idpais`),
  KEY `FK_paises` (`idcontinente`),
  CONSTRAINT `FK_paises` FOREIGN KEY (`idcontinente`) REFERENCES `continentes` (`idcontinente`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;

/*Data for the table `paises` */

insert  into `paises`(`idpais`,`idcontinente`,`pais`,`iso2`,`iso3`) values (1,7,'Canadá','CA','CAN'),(2,7,'Bahamas','BS','BHS'),(3,7,'Barbados','BB','BRB'),(4,7,'Bermudas','BM','BMU'),(5,7,'Islas Caimán','KY','CYM'),(6,7,'Cuba','CU','CUB'),(7,7,'República Dominicana','DO','DOM'),(8,7,'Argentina','AR','ARG'),(9,7,'Bolivia','BO','BOL'),(10,7,'Brasil','BR','BRA'),(11,7,'Chile','CL','CHL'),(12,7,'Colombia','CO','COL'),(13,7,'Belice','BZ','BLZ'),(14,7,'Costa Rica','CR','CRI'),(15,5,'Bulgaria','BG','BGR'),(16,5,'Bielorrusia','BY','BLR'),(17,5,'Croacia','HR','HRV'),(18,5,'Chipre','CY','CYP'),(19,5,'República Checa','CZ','CZE'),(20,5,'Dinamarca','DK','DNK'),(21,5,'Estonia','EE','EST'),(22,5,'Francia','FR','FRA'),(23,5,'Georgia','GE','GEO'),(24,5,'Alemania','DE','DEU'),(25,5,'Gibraltar','GI','GIB'),(26,5,'Grecia','GR','GRC'),(27,3,'Kenia','KE','KEN'),(28,3,'Lesotho','LS','LSO'),(29,3,'Liberia','LR','LBR'),(30,3,'Libia','LY','LBY'),(31,3,'Madagascar','MG','MDG'),(32,3,'Malaui','MW','MWI'),(33,3,'Mauritania','MR','MRT'),(34,3,'Mauricio','MU','MUS'),(35,3,'Marruecos','MA','MAR'),(36,3,'Mozambique','MZ','MOZ'),(37,4,'Hong Kong','HK','HKG'),(38,4,'India','IN','IND'),(39,4,'Indonesia','ID','IDN'),(40,4,'Irán','IR','IRN'),(41,7,'Estados Unidos','US','USA'),(42,7,'Trinidad y Tobago','TT','TTO'),(43,7,'Surinam','SR','SUR'),(44,7,'Uruguay','UY','URY'),(45,7,'Venezuela','VE','VEN'),(46,7,'Islas Malvinas','FK','FLK'),(47,7,'El Salvador','SV','SLV'),(48,7,'Guatemala','GT','GTM'),(49,4,'Iraq','IQ','IRQ'),(50,4,'Israel','IL','ISR'),(51,4,'Japón','JP','JPN'),(52,4,'Kazajstán','KZ','KAZ'),(53,4,'Jordania','JO','JOR'),(54,4,'Corea del Norte','KP','PRK'),(55,4,'Corea del Sur','KR','KOR'),(56,4,'Kuwait','KW','KWT'),(57,4,'Kirguistán','KG','KGZ'),(58,4,'Laos','LA','LAO'),(59,4,'Líbano','LB','LBN'),(60,4,'Macao','MO','MAC'),(61,4,'Malasia','MY','MYS'),(62,4,'Maldivas','MV','MDV'),(63,5,'Hungría','HU','HUN'),(64,5,'Islandia','IS','ISL'),(65,5,'Irlanda','IE','IRL'),(66,5,'Italia','IT','ITA'),(67,5,'Letonia','LV','LVA'),(68,5,'Lituania','LT','LTU'),(69,5,'Luxemburgo','LU','LUX'),(70,5,'Malta','MT','MLT'),(71,5,'Moldavia','MD','MDA'),(72,5,'Países Bajos','NL','NLD'),(73,5,'Noruega','NO','NOR'),(74,5,'Polonia','PL','POL'),(75,5,'Portugal','PT','PRT'),(76,5,'Rumania','RO','ROU'),(77,2,'Fiyi','FJ','FJI'),(78,2,'Vanuatu','VU','VUT'),(79,2,'Nueva Zelanda','NZ','NZL'),(80,2,'Papúa Nueva Guinea','PG','PNG'),(81,2,'Tonga','TO','TON'),(82,2,'Samoa','WS','WSM'),(83,3,'Comoras','KM','COM'),(84,3,'República Democrática del Congo','CD','COD'),(85,3,'Etiopía','ET','ETH'),(86,7,'México','MX','MEX'),(87,7,'Haití','HT','HTI'),(88,7,'Jamaica','JM','JAM'),(89,7,'Antillas Holandesas','AN','ANT'),(90,7,'Aruba','AW','ABW'),(91,7,'Guyana','GY','GUY'),(92,7,'Paraguay','PY','PRY'),(93,7,'Perú','PE','PER'),(94,7,'Honduras','HN','HND'),(95,7,'Nicaragua','NI','NIC'),(96,7,'Panamá','PA','PAN'),(97,3,'Eritrea','ER','ERI'),(98,3,'Yibuti','DJ','DJI'),(99,3,'Gambia','GM','GMB'),(100,3,'Ghana','GH','GHA'),(101,3,'Guinea','GN','GIN'),(102,3,'Namibia','NA','NAM'),(103,3,'Nigeria','NG','NGA'),(104,3,'Ruanda','RW','RWA'),(105,3,'Santa Helena','SH','SHN'),(106,4,'Siria','SY','SYR'),(107,4,'Tayikistán','TJ','TJK'),(108,4,'Tailandia','TH','THA'),(109,4,'Emiratos Árabes Unidos','AE','ARE'),(110,4,'Turquía','TR','TUR'),(111,4,'Turkmenistán','TM','TKM'),(112,4,'Uzbekistán','UZ','UZB'),(113,4,'Yemen','YE','YEM'),(114,5,'Eslovaquia','SK','SVK'),(115,5,'España','ES','ESP'),(116,5,'Suecia','SE','SWE'),(117,5,'Suiza','CH','CHE'),(118,5,'Ucrania','UA','UKR'),(119,5,'Macedonia','MK','MKD'),(120,5,'Reino Unido','GB','GBR'),(121,3,'Sudán','SD','SDN'),(122,3,'Suazilandia','SZ','SWZ'),(123,2,'Australia','AU','AUS'),(124,2,'Islas Salomón','SB','SLB'),(125,3,'Argelia','DZ','DZA'),(126,3,'Angola','AO','AGO'),(127,3,'Botsuana','BW','BWA'),(128,3,'Burundi','BI','BDI'),(129,3,'Cabo Verde','CV','CPV'),(130,4,'Afganistán','AF','AFG'),(131,4,'Azerbaiyán','AZ','AZE'),(132,4,'Bahréin','BH','BHR'),(133,4,'Bangladesh','BD','BGD'),(134,4,'Armenia','AM','ARM'),(135,4,'Bhután','BT','BTN'),(136,3,'Santo Tomé y Príncipe','ST','STP'),(137,3,'Seychelles','SC','SYC'),(138,3,'Sierra Leona','SL','SLE'),(139,3,'Somalia','SO','SOM'),(140,3,'Sudáfrica','ZA','ZAF'),(141,3,'Zimbabue','ZW','ZWE'),(142,4,'Arabia Saudí','SA','SAU'),(143,4,'Singapur','SG','SGP'),(144,4,'Vietnam','VN','VNM'),(145,4,'Brunéi','BN','BRN'),(146,4,'Myanmar','MM','MMR'),(147,4,'Camboya','KH','KHM'),(148,4,'Sri Lanka','LK','LKA'),(149,4,'China','CN','CHN'),(150,4,'Taiwán','TW','TWN'),(151,5,'Albania','AL','ALB'),(152,5,'Andorra','AD','AND'),(153,5,'Austria','AT','AUT'),(154,5,'Bélgica','BE','BEL'),(155,5,'Bosnia y Herzegovina','BA','BIH'),(156,5,'Finlandia','FI','FIN'),(157,3,'Túnez','TN','TUN'),(158,3,'Uganda','UG','UGA'),(159,3,'Egipto','EG','EGY'),(160,3,'Tanzania','TZ','TZA'),(161,3,'Zambia','ZM','ZMB'),(162,4,'Mongolia','MN','MNG'),(163,4,'Omán','OM','OMN'),(164,4,'Nepal','NP','NPL'),(165,4,'Pakistán','PK','PAK'),(166,4,'Filipinas','PH','PHL'),(167,4,'Qatar','QA','QAT'),(168,4,'Rusia','RU','RUS'),(169,7,'Antigua y Barbuda','AG','ATG'),(170,7,'Islas Vírgenes Británicas','VG','VGB'),(171,7,'Dominica','DM','DMA'),(172,7,'Ecuador','EC','ECU'),(173,5,'Mónaco','MC','MCO'),(174,7,'San Pedro y Miquelón','PM','SPM'),(175,7,'Martinica','MQ','MTQ'),(176,7,'Montserrat','MS','MSR'),(177,7,'Puerto Rico','PR','PRI'),(178,7,'San Cristóbal y Nieves','KN','KNA'),(179,7,'Anguila','AI','AIA'),(180,7,'Santa Lucía','LC','LCA'),(181,7,'San Vicente y las Granadinas','VC','VCT'),(182,2,'Samoa Americana','AS','ASM'),(183,3,'Camerún','CM','CMR'),(184,3,'República Centroafricana','CF','CAF'),(185,3,'Chad','TD','TCD'),(186,3,'Mayotte','YT','MYT'),(187,4,'Palestina','PS','PSE'),(188,5,'Islas Feroe','FO','FRO'),(189,5,'Islas Gland','AX','ALA'),(190,2,'Islas Cocos','CC','CCK'),(191,8,'Antártida','AQ','ATA'),(192,2,'Islas Heard y McDonald','HM','HMD'),(193,3,'Costa de Marfil','CI','CIV'),(194,3,'Malí','ML','MLI'),(195,3,'Níger','NE','NER'),(196,7,'Groenlandia','GL','GRL'),(197,7,'Islas Turcas y Caicos','TC','TCA'),(198,7,'Islas Vírgenes de los Estados Unidos','VI','VIR'),(199,7,'Granada','GD','GRD'),(200,7,'Guadalupe','GP','GLP'),(201,7,'Islas Georgias del Sur y Sandwich del Sur','GS','SGS'),(202,7,'Guayana Francesa','GF','GUF'),(203,5,'Ciudad del Vaticano','VA','VAT'),(204,5,'Liechtenstein','LI','LIE'),(205,5,'Montenegro','ME','MNE'),(206,1,'Isla Bouvet','BV','BVT'),(207,1,'Territorio Británico del Océano Índico','IO','IOT'),(208,1,'Territorios Australes Franceses','TF','ATF'),(209,1,'Islas Ultramarinas de Estados Unidos','UM','UMI'),(210,2,'Isla de Navidad','CX','CXR'),(211,2,'Islas Cook','CK','COK'),(212,2,'Polinesia Francesa','PF','PYF'),(213,2,'Kiribati','KI','KIR'),(214,2,'Guam','GU','GUM'),(215,2,'Nauru','NR','NRU'),(216,2,'Nueva Caledonia','NC','NCL'),(217,2,'Niue','NU','NIU'),(218,2,'Isla Norfolk','NF','NFK'),(219,2,'Islas Marianas del Norte','MP','MNP'),(220,2,'Micronesia','FM','FSM'),(221,2,'Islas Marshall','MH','MHL'),(222,2,'Palaos','PW','PLW'),(223,2,'Islas Pitcairn','PN','PCN'),(224,2,'Tokelau','TK','TKL'),(225,2,'Tuvalu','TV','TUV'),(226,2,'Wallis y Futuna','WF','WLF'),(227,3,'Congo','CG','COG'),(228,3,'Benín','BJ','BEN'),(229,3,'Guinea Ecuatorial','GQ','GNQ'),(230,3,'Gabón','GA','GAB'),(231,3,'Guinea-Bissau','GW','GNB'),(232,3,'Reunión','RE','REU'),(233,3,'Senegal','SN','SEN'),(234,3,'Sahara Occidental','EH','ESH'),(235,5,'San Marino','SM','SMR'),(236,5,'Serbia','RS','SRB'),(237,5,'Eslovenia','SI','SVN'),(238,5,'Svalbard y Jan Mayen','SJ','SJM'),(239,3,'Togo','TG','TGO'),(240,3,'Burkina Faso','BF','BFA'),(241,4,'Timor Oriental','TL','TLS');

/*Table structure for table `tipologias` */

DROP TABLE IF EXISTS `tipologias`;

CREATE TABLE `tipologias` (
  `idtipologia` double NOT NULL AUTO_INCREMENT COMMENT 'Clave principal de la tabla',
  `descripcion` varchar(250) NOT NULL COMMENT 'Nombre o descripción de la tipología',
  `siglas` varchar(4) DEFAULT NULL COMMENT 'siglas representativas de la tipología',
  PRIMARY KEY (`idtipologia`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tipologias` */

insert  into `tipologias`(`idtipologia`,`descripcion`,`siglas`) values (1,'Sociedad anónima','SA'),(2,'Compañía anónima','CA'),(3,'Organización sin fines de Lucro','OSFL'),(4,'Organización No Gubernamental','ONG');

/* Procedure structure for procedure `actualizar_compania` */

/*!50003 DROP PROCEDURE IF EXISTS  `actualizar_compania` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_compania`(
 IN idempresa_param double,
 IN idtipologia_param DOUBLE,
 IN idpais_param double,
 IN nombre_param VARCHAR(250),
 IN estado_param VARCHAR(100),
 IN ciudad_param VARCHAR(100),
 IN usuario_param VARCHAR(250),
 IN password_param VARCHAR(200),
 IN descripcion_param text,
 IN fecha_actualizacion_param datetime 
 )
BEGIN
 update empresas set idtipologia=idtipologia_param,idpais=idpais_param,nombre=nombre_param,estado=estado_param,ciudad=ciudad_param,usuario=usuario_param,PASSWORD=password_param,descripcion=descripcion_param,fecha_actualizacion=fecha_actualizacion_param where idempresa=idempresa_param;
END */$$
DELIMITER ;

/* Procedure structure for procedure `consultar_cantidad_empresa_avanzada` */

/*!50003 DROP PROCEDURE IF EXISTS  `consultar_cantidad_empresa_avanzada` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_cantidad_empresa_avanzada`(busqueda VARCHAR(250),ordenNombre INT, ordenPais INT)
BEGIN
		/*como no se que verion tienen de mysql no me atrevo a usar concat sobre la sql que requiere una version reciente de mysql, adopto esta forma de consultar con if*/	
	IF ordenNombre >0 OR ordenPais>0 THEN 
		IF ordenNombre >0 THEN 
			IF ordenNombre=1 THEN
				IF busqueda <> "" THEN
					SELECT  count(empresas.idempresa) as cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY nombre ASC;					
				ELSE
					SELECT  COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY nombre ASC;				
				END IF;
			ELSE
				IF busqueda <> "" THEN
					SELECT  COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY nombre DESC;
				ELSE					
					SELECT  COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY nombre DESC;				
				END IF;
			END IF;
		ELSE
			IF ordenPais=1 THEN
				IF busqueda <> "" THEN					
					SELECT  COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY paises.pais ASC;					
				ELSE
					SELECT  COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY paises.pais ASC;				
				END IF;
			ELSE
				IF busqueda <> "" THEN					
					SELECT COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY paises.pais DESC;					
				ELSE
					
					SELECT COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY paises.pais DESC;									
				END IF;
			END IF;
		
		END IF;
	ELSE
	/*Sin orden */
		IF busqueda <> "" THEN			
			SELECT COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE empresas.nombre LIKE CONCAT('%',busqueda,'%');			
		ELSE			
			SELECT  COUNT(empresas.idempresa) AS cantidad FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia);							
		END IF;
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `consultar_empresa` */

/*!50003 DROP PROCEDURE IF EXISTS  `consultar_empresa` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_empresa`(idempresa_param double)
BEGIN
SELECT idempresa,idtipologia,idpais,nombre,estado,ciudad,usuario,descripcion,fecha_registro,fecha_actualizacion FROM empresas WHERE idempresa=idempresa_param;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `consultar_empresa_avanzada` */

/*!50003 DROP PROCEDURE IF EXISTS  `consultar_empresa_avanzada` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_empresa_avanzada`(busqueda VARCHAR(250),pagina INT,cantidad INT, ordenNombre INT, ordenPais INT)
BEGIN
		/*como no se que verion tienen de mysql no me atrevo a usar concat sobre la sql que requiere una version reciente de mysql, adopto esta forma de consultar con if*/	
	IF ordenNombre >0 OR ordenPais>0 THEN 
		IF ordenNombre >0 THEN 
			IF ordenNombre=1 THEN
				IF busqueda <> "" THEN
					SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas inner join paises on (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY nombre ASC LIMIT pagina,cantidad;
				ELSE
					SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY nombre ASC LIMIT pagina,cantidad;	
				END IF;
			ELSE
				IF busqueda <> "" THEN
					SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY nombre DESC LIMIT pagina,cantidad;
				ELSE
					SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY nombre DESC LIMIT pagina,cantidad;				
				END IF;
			END IF;
		ELSE
			IF ordenPais=1 THEN
				IF busqueda <> "" THEN
					SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY pais ASC LIMIT pagina,cantidad;
				ELSE
					SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY pais ASC LIMIT pagina,cantidad;					
				END IF;
			ELSE
				IF busqueda <> "" THEN
					SELECT empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE nombre LIKE CONCAT('%',busqueda,'%') ORDER BY pais DESC LIMIT pagina,cantidad;
				ELSE
					SELECT empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) ORDER BY pais DESC LIMIT pagina,cantidad;
				END IF;
			END IF;
		
		END IF;
	ELSE
	/*Sin orden */
		IF busqueda <> "" THEN			
			SELECT empresas.idempresa,empresas.idtipologia,tipologias.descripcion aS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) WHERE empresas.nombre LIKE CONCAT('%',busqueda,'%') LIMIT pagina,cantidad;
		ELSE			
			SELECT  empresas.idempresa,empresas.idtipologia,tipologias.descripcion AS tipologia,paises.pais AS pais,empresas.idpais,empresas.nombre,empresas.estado,empresas.ciudad,empresas.usuario,empresas.descripcion,empresas.fecha_registro,empresas.fecha_actualizacion FROM empresas INNER JOIN paises ON (paises.idpais=empresas.idpais) INNER JOIN tipologias ON (tipologias.idtipologia=empresas.idtipologia) LIMIT pagina,cantidad;				
		END IF;
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `consultar_usuario_compania` */

/*!50003 DROP PROCEDURE IF EXISTS  `consultar_usuario_compania` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_usuario_compania`(usuario_param varchar(250),password_param varchar(250))
BEGIN
	SELECT idempresa,nombre,usuario,idpais,estado,ciudad FROM empresas WHERE usuario=usuario_param AND PASSWORD=password_param;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `consultar_usuario_existencia` */

/*!50003 DROP PROCEDURE IF EXISTS  `consultar_usuario_existencia` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_usuario_existencia`(usuario_param VARCHAR(250))
BEGIN
	SELECT idempresa FROM empresas WHERE usuario=usuario_param limit 1;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `insertar_compania` */

/*!50003 DROP PROCEDURE IF EXISTS  `insertar_compania` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_compania`(
 IN idtipologia_param double,
 IN idpais_param double,
 IN nombre_param VARCHAR(250),
 IN estado_param VARCHAR(100),
 IN ciudad_param VARCHAR(100),
 IN usuario_param VARCHAR(250),
 IN password_param VARCHAR(200),
 IN descripcion_param text,
 IN fecha_registro_param datetime 
 )
BEGIN
 insert into  empresas(idtipologia,idpais,nombre,estado,ciudad,usuario,PASSWORD,descripcion,fecha_registro) values (idtipologia_param,idpais_param,nombre_param,estado_param,ciudad_param,usuario_param,password_param,descripcion_param,fecha_registro_param);
END */$$
DELIMITER ;

/*Table structure for table `view_paises` */

DROP TABLE IF EXISTS `view_paises`;

/*!50001 DROP VIEW IF EXISTS `view_paises` */;
/*!50001 DROP TABLE IF EXISTS `view_paises` */;

/*!50001 CREATE TABLE `view_paises` (
  `idpais` double NOT NULL DEFAULT '0' COMMENT 'Clave principal de la tabla',
  `continente` varchar(250) NOT NULL COMMENT 'Nombre del continente',
  `pais` varchar(250) NOT NULL COMMENT 'Nombre del país',
  `iso2` varchar(2) NOT NULL COMMENT 'código ISO internacional de dos dígitos del país',
  `iso3` varchar(3) NOT NULL COMMENT 'código ISO internacional de tres dígitos del país'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 */;

/*Table structure for table `view_tipologias` */

DROP TABLE IF EXISTS `view_tipologias`;

/*!50001 DROP VIEW IF EXISTS `view_tipologias` */;
/*!50001 DROP TABLE IF EXISTS `view_tipologias` */;

/*!50001 CREATE TABLE `view_tipologias` (
  `idtipologia` double NOT NULL DEFAULT '0' COMMENT 'Clave principal de la tabla',
  `descripcion` varchar(250) NOT NULL COMMENT 'Nombre o descripción de la tipología',
  `siglas` varchar(4) DEFAULT NULL COMMENT 'siglas representativas de la tipología'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 */;

/*View structure for view view_paises */

/*!50001 DROP TABLE IF EXISTS `view_paises` */;
/*!50001 DROP VIEW IF EXISTS `view_paises` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_paises` AS (select `paises`.`idpais` AS `idpais`,`continentes`.`nombre` AS `continente`,`paises`.`pais` AS `pais`,`paises`.`iso2` AS `iso2`,`paises`.`iso3` AS `iso3` from (`paises` join `continentes` on((`paises`.`idcontinente` = `continentes`.`idcontinente`))) order by `paises`.`idpais`,`continentes`.`nombre`,`paises`.`pais`) */;

/*View structure for view view_tipologias */

/*!50001 DROP TABLE IF EXISTS `view_tipologias` */;
/*!50001 DROP VIEW IF EXISTS `view_tipologias` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_tipologias` AS (select `tipologias`.`idtipologia` AS `idtipologia`,`tipologias`.`descripcion` AS `descripcion`,`tipologias`.`siglas` AS `siglas` from `tipologias` order by `tipologias`.`idtipologia`) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
