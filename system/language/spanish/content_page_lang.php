<?php
/**
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['home'] = 'INICIO';
$lang['company'] = 'EMPRESAS';
$lang['services'] = 'SERVICIOS';
$lang['price'] = 'VALORA UN SERVICIO';
$lang['contact'] = 'CONTACTO';
$lang['register'] = 'Registro';
$lang['init-session'] = 'Iniciar sesi&oacute;n';
$lang['save-changes'] = 'Guardar cambios';
$lang['where'] = 'Est&aacute;s en';
$lang['basic-information'] = 'INFORMACI&Oacute;N B&Aacute;SICA';
$lang['company-name'] = 'Nombre comercial de la empresa';
$lang['typology'] = 'Tipolog&iacute;a de la empresa';
$lang['country'] = 'Pa&iacute;s';
$lang['state'] = 'Estado';
$lang['city'] = 'Ciudad';
$lang['register-information'] = 'INFORMACI&OACUTE;N REGISTRO';
$lang['mail'] = 'Email';
$lang['user'] = 'Usuario';
$lang['password'] = 'Password';
$lang['repeat-password'] = 'Repetir Password';
$lang['company-description'] = 'Descripci&oacute;n de la empresa';
$lang['opinion'] = 'Opiniones y consejos sobre servicios sanitarios, funerarios y muchos más.';
$lang['copyright'] = 'ALIFE Todos los derechos reservados. ALIFE Condiciones de uso, Política de privacidad y Política de cookies.Sed tincidunt velit a accumsan laoreet. Donec vitae est quis ante rhoncus cursus non id ante. Nam fringilla euismod dui, non ullamcorper nunc. Nullam id vestibulum orci, id porttitor lectus. Maecenas scelerisque dignissim erat et faucibus. Cras malesuada non massa ac lobortis.';
$lang['own'] = 'Veritus';
