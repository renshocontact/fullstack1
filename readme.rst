**FULLSTACK**

**IMPORTANTE**: Desarrollo utilizando CodeIgniter, MYSQL, BootStrap, HTML5

**Parte única**

* Una vez registrado, puede iniciar sesión y acceder a la listas de empresas, sólo pudiendo modificar la suya
* El objetivo es mostrar un código de desarrollo, filosofía, organización y como trabajo. LO MÁS IMPORTANTE ORGANIZACIÓN Y CÓDIGO LIMPIO. Fijarse en el archivo de constants, un pilar del código.
* Hay un **store procedure** que posee una serie de if anidados, la mejor solución es concatenar dinámicamente la SQL y evitar el especie de 'CALLBACK HELL', pero enfrentamos el problema de la versión de MYSQL que lo soporta, para asegurar que a todos los usuarios les corra el ejemplo indistintamente de la versión MYSQL instalada, opté por anidar las combinaciones posibles.