$(document).ready(function() {
  $('a[data-confirm]').click(function(ev) {
    var href = $(this).attr('href');
    if (!$('#confirmModal').length) {
      $('body').append('<div id="confirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel"></h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" id="dataCancel" aria-hidden="true"></button><a class="btn btn-primary" id="dataConfirm"></a></div></div></div></div>');
    }
    $('#confirmModal').find('.modal-body').text($(this).attr('data-confirm-content'));
    $('#confirmModal').find('#dataConfirmLabel').text($(this).attr('data-header'));
    $('#dataConfirm').text($(this).attr('data-confirm'));
    $('#dataCancel').text($(this).attr('data-cancel'))
    $('#dataConfirm').attr('href', href);
    $('#confirmModal').modal({show:true});
    return false;
  });
});
